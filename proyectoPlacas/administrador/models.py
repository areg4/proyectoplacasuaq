#archivo de los modelos del sistema de placas
from django.db import models

#opciones de los tipos de usuario validos
opcionesTipousuario = (
    ('Administrador','Administrador'),
    ('Usuario', 'Usuario')
)
#tabla de usuarios
class Usuarios(models.Model):
    nombreUsuario = models.CharField(max_length=50)
    apellidosUsuario = models.CharField(max_length=50)
    usuario = models.CharField(max_length=25)
    password = models.CharField(max_length=25)
    tipoUsuario = models.CharField(max_length=15,choices=opcionesTipousuario)
    habilitado = models.PositiveSmallIntegerField(default=1)

#tabla de camppus
class Campus(models.Model):
    nombreCampus = models.CharField(max_length=100)
    direccionCampus = models.CharField(max_length=100)
    habilitado = models.PositiveSmallIntegerField(default=1)

#opciones de los tipos de entradas
opcionesEntrada = (
    ('Entrada', 'Entrada'),
    ('Salida', 'Salida'),
    ('Ambos','Ambos')
)
#tabla de entradas
class Entradas(models.Model):
    campus = models.ForeignKey(Campus,on_delete=models.CASCADE)
    direccionEntrada = models.CharField(max_length=200)
    tipoEntrada = models.CharField(max_length=8,choices=opcionesEntrada)
    habilitado = models.PositiveSmallIntegerField(default=1)

#tabla de cámaras
class Camaras(models.Model):
    entrada = models.ForeignKey(Entradas, on_delete=models.CASCADE)
    direccionIP = models.CharField(max_length=30)
    detallesCamara = models.TextField(max_length=250)
    orientacionCamara = models.CharField(max_length=50)
    habilitado = models.PositiveSmallIntegerField(default=1)

#tabla de propietarios de vehículos
class Propietarios(models.Model):
    nombrePropietario = models.CharField(max_length=50)
    apellidosPropietario = models.CharField(max_length=100)
    expediente = models.PositiveIntegerField(unique=True)
    facultad = models.CharField(max_length=50)
    carrera = models.CharField(max_length=50)
    habilitado = models.PositiveSmallIntegerField(default=1)

# tabla de vehículos
class Vehiculos(models.Model):
    propietario = models.ForeignKey(Propietarios, on_delete=models.CASCADE)
    marca = models.CharField(max_length=50)
    detalles = models.TextField(max_length=250)
    placa = models.CharField(max_length=20)
    habilitado = models.PositiveSmallIntegerField(default=1)

#tabla de usuarios y campus
class UsuarioCampus(models.Model):
    usuario = models.ForeignKey(Usuarios, on_delete=models.CASCADE)
    campus = models.ForeignKey(Campus, on_delete=models.CASCADE)
    habilitado = models.PositiveSmallIntegerField(default=1)

#tabla de los accesos de los carros
class Accesos(models.Model):
    placa = models.CharField(max_length=20)
    entradaAcceso = models.DateTimeField()
    salidaAcceso = models.DateTimeField(null=True)
    entrada = models.ForeignKey(Entradas, on_delete=models.CASCADE)
    habilitado = models.PositiveSmallIntegerField(default=1)
