#Archivo de rutas de la app de admin
from django.urls import path
from . import views

urlpatterns = [
    path('',views.login, name='login'),
    path('monitoreo', views.monitoreo, name='monitoreoAdmin'),
    path('usuarios', views.usuarios, name='usuariosAdmin'),
    path('campus', views.campus, name="campusAdmin"),
    path('entradas', views.entradas, name="entradasAdmin"),
    path('camaras', views.camaras, name="camarasAdmin"),
    path('propietarios', views.propietarios, name="propietariosAdmin"),
    path('vehiculos', views.vehiculos, name="vehiculosAdmin"),
    path('reportes', views.reportes, name="reportesAdmin"),
    path('busqueda', views.busqueda, name="busquedaAdmin"),
    # path('login/',views.login, name='login')
]