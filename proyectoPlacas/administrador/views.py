from django.shortcuts import render
from django.http import HttpResponse
from django.shortcuts import redirect
from django.contrib.auth.decorators import login_required

pS = "Administrador"

@login_required
def login(request):
    if request.user.is_authenticated:
        grupoUsuario = request.user.groups.all()[0]
        # print(grupoUsuario)
        if (str(grupoUsuario) == pS):
            return redirect('/administrador/monitoreo')
            # return HttpResponse('Hola Administrador')
        if(str(grupoUsuario) == 'Usuario'):
            # print('entra')
            return redirect('/usuario/monitoreo')
            # return HttpResponse('Hola Usuario')
    else:
        return redirect('/accounts/login')

@login_required
def monitoreo(request):
    gU = str(request.user.groups.all()[0])
    if (request.user.is_authenticated) and (gU == pS):
        # return HttpResponse('Pantalla Monitoreo Administrador')
        return render(request,'admin/monitoreo.html')
    else:
        return redirect('/')

@login_required
def usuarios(request):
    gU = str(request.user.groups.all()[0])
    if (request.user.is_authenticated) and (gU == pS):
        return HttpResponse('Pantalla Usuarios Administrador')
    else:
        return redirect('/')

@login_required
def campus(request):
    gU = str(request.user.groups.all()[0])
    if (request.user.is_authenticated) and (gU == pS):
        return HttpResponse('Pantalla Campus Administrador')
    else:
        return redirect('/')

@login_required
def entradas(request):
    gU = str(request.user.groups.all()[0])
    if (request.user.is_authenticated) and (gU == pS):
        return HttpResponse('Pantalla Entradas Administrador')
    else:
        return redirect('/')

@login_required
def camaras(request):
    gU = str(request.user.groups.all()[0])
    if (request.user.is_authenticated) and (gU == pS):
        return HttpResponse('Pantalla Cámaras Administrador')
    else:
        return redirect('/')

@login_required
def propietarios(request):
    gU = str(request.user.groups.all()[0])
    if (request.user.is_authenticated) and (gU == pS):
        return HttpResponse('Pantalla Propietarios Administrador')
    else:
        return redirect('/')

@login_required
def vehiculos(request):
    gU = str(request.user.groups.all()[0])
    if (request.user.is_authenticated) and (gU == pS):
        return HttpResponse('Pantalla Vehículos Administrador')
    else:
        return redirect('/')

@login_required
def reportes(request):
    gU = str(request.user.groups.all()[0])
    if (request.user.is_authenticated) and (gU == pS):
        return HttpResponse('Pantalla Reportes Administrador')
    else:
        return redirect('/')

def busqueda(request):
    gU = str(request.user.groups.all()[0])
    if (request.user.is_authenticated) and (gU == pS):
        return HttpResponse('Pantalla Búsqueda Administrador')
    else:
        return redirect('/')