from django.shortcuts import render
from django.http import HttpResponse
from django.shortcuts import redirect
from django.contrib.auth.decorators import login_required

pS = "Usuario"

def index(request):
    return redirect('/')

@login_required
def monitoreo(request):
    gU = str(request.user.groups.all()[0])
    if (request.user.is_authenticated) and (gU == pS):
        # return HttpResponse('Pantalla Monitoreo Usuario')
        return render(request, 'user/monitoreo.html')
    else:
        return redirect('/')

@login_required
def entradas(request):
    gU = str(request.user.groups.all()[0])
    if (request.user.is_authenticated) and (gU == pS):
        return HttpResponse('Pantalla Entradas Usuario')
    else:
        return redirect('/')

@login_required
def camaras(request):
    gU = str(request.user.groups.all()[0])
    if (request.user.is_authenticated) and (gU == pS):
        return HttpResponse('Pantalla Cámaras Usuario')
    else:
        return redirect('/')

@login_required
def propietarios(request):
    gU = str(request.user.groups.all()[0])
    if (request.user.is_authenticated) and (gU == pS):
        return HttpResponse('Pantalla Propietarios Usuario')
    else:
        return redirect('/')

@login_required
def reportes(request):
    gU = str(request.user.groups.all()[0])
    if (request.user.is_authenticated) and (gU == pS):
        return HttpResponse('Pantalla Reportes Usuario')
    else:
        return redirect('/')

@login_required
def busqueda(request):
    gU = str(request.user.groups.all()[0])
    if (request.user.is_authenticated) and (gU == pS):
        return HttpResponse('Pantalla Búsqueda Usuario')
    else:
        return redirect('/')