# Archivo de rutas de la app  de usuarios
from django.urls import path
from . import views

urlpatterns = [
    path('',views.index, name='login'),
    path('monitoreo',views.monitoreo, name='monitoreoUsuario'),
    path('entradas', views.entradas, name="entradasUsuario"),
    path('camaras', views.camaras, name="camarasUsuario"),
    path('propietarios', views.propietarios, name="propietariosUsuario"),
    path('reportes', views.reportes, name="reportesUsuario"),
    path('busqueda', views.busqueda, name="busquedaUsuario"),
]